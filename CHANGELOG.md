# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.2.1

- patch: SQRP-120 Mention SonarQube Community Build in the README

## 1.2.0

- minor: SONAR-23648 Rebrand repositories owned by the Analysis Experience squad

## 1.1.0

- minor: Update base image, fix README logo, generate SBOM

## 1.0.0

- major: Initial version

