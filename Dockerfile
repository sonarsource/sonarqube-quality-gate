FROM alpine:3.14
RUN apk update && apk add bash curl jq

COPY pipe /usr/bin/
RUN chmod 755 /usr/bin/check-quality-gate.sh

ENTRYPOINT ["/usr/bin/check-quality-gate.sh"]
